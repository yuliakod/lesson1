package zoo;

import java.util.ArrayList;


import zoo.animals.*;

public class Zoo {
	private String name;
	
	public Zoo(String name) {
		this.name = name;
	}

	private ArrayList<Animal> animalsList = new ArrayList<Animal>();

	public void addAnimal(Animal anim) {
		animalsList.add(anim);
	}
	public void removeAnimal(Animal anim){
		animalsList.remove(anim);
		}

	public void printAll() {
		for (int i = 0; i < animalsList.size(); i++) {
			Animal animal = animalsList.get(i);
			System.out.println(animal.toString());
		}
	}
	public void removeAllAnimals() {
		animalsList.clear();
	}

	public int getSize() {
		return animalsList.size();

	}

	public int getAnimalsSummaryWeight() {
		    int summaryWeight=0;
			for (int i = 0; i < animalsList.size(); i++) {
				Animal animal=animalsList.get(i);
				summaryWeight=summaryWeight+animal.getWeight();
		}
			return summaryWeight;
	}

	public int getWildAnimalsAge() {
		int summaryAge = 0;
		for (int i = 0; i < animalsList.size(); i++) {
			if (animalsList.get(i) instanceof WildAnimals) {
				Animal animal = animalsList.get(i);
				summaryAge = summaryAge + animal.getAge();
			}
			
		}
		return summaryAge;
	}
	
	@Override
	public String toString() {
		return name;
	}
}  
