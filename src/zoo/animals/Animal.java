package zoo.animals;


public class Animal {
	protected int age;
	protected String color;
	protected int weight;
	
	
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	
	
	public void setWeight(int weight){
		this.weight = weight;
	}
	public int getWeight(){
		return weight;
		
	}
	@Override
	public String toString() {
		return "Animal [age=" + age + ", color=" + color + ", weight=" + weight + "]";
	}
	
	public void smile(String color){
		System.out.println("My smile is"+color);
	}

	
}
